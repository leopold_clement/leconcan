import numpy as np
import matplotlib.pyplot as plt

N = 3
nu = 2**N
num = nu-1

# Quantification idéal
fig, ax = plt.subplots(1, 1, figsize=(3,3))

ax.step(np.linspace(0, 1, nu), range(nu), "k",where='mid')
ax.plot(np.linspace(0, 1, nu), range(nu), 'kx')

ax.set_xlim(0,1)
ax.set_ylim(0,num)
ax.set_xlabel('$ \\frac{V}{V_{max}}$')
ax.set_ylabel('Code')
ax.grid()
fig.tight_layout()
fig.savefig('quantification_ideal.pgf', transparent=True)

# Pleinne echelle
fig, ax = plt.subplots(1, 1, figsize=(2,2))

ax.step(np.linspace(0, 1, nu), range(nu), where='mid', label='$0$ à $V_\\text{max}$')
ax.step(np.linspace(-1, 1, nu), range(nu), where='mid', label='$ - V_\\text{max}$ à $V_\\text{max}$')
ax.set_xlim(-1,1)
ax.set_ylim(0,num)
ax.set_xlabel('$ \\frac{V}{V_{max}}$')
ax.set_ylabel('Code')
ax.grid()

fig.tight_layout()
fig.savefig('pleinne_echelle.pgf', transparent=True)

# Erreur idéal
fig, ax = plt.subplots(1, 1, figsize=(2.5,2.5))
x = np.linspace(0,1, nu*100)
e = num*x- np.array([round(num*dx) for dx in x])
ax.plot(x, e/num)

ax.set_xlim(0,1)
ax.set_xlabel('$ \\frac{V}{V_{max}}$')
ax.set_ylabel('$ \\frac{\\epsilon}{V_{max}}$')
ax.grid()
fig.tight_layout()
fig.savefig('erreur_ideal.pgf', transparent=True)
